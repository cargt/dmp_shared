/***********************************************************
*
* RfidReaderQueue.h
*
* RfidReaderQueue API.  A fixed length FIFO queue for WiegandData.
*
* "Copyright (c) Digital Monitoring Products Inc. 2012."
* "All rights reserved."
*
***********************************************************/


#ifndef _RFIDREADERQUEUE_H_
#define _RFIDREADERQUEUE_H_


#include <stdbool.h>
#include "Wiegand.h"



/** Initialize the queue.
 */
void RfidReaderQueue_Init(void);


/** Get the maximum number of elements the queue can contain.
 */
size_t RfidReaderQueue_GetMaxElements(void);


/** Get the number of elements currently in container.
 */
size_t RfidReaderQueue_GetCount(void);


/** Push event on the queue.
 */
bool RfidReaderQueue_Push(WiegandData_t event);


/** Pop an event off the queue.
 */
WiegandData_t RfidReaderQueue_Pop();


/** Peek at next event.
 */
WiegandData_t RfidReaderQueue_Peek();


/** Returns true if the queue is empty.
 */
bool RfidReaderQueue_IsEmpty(void);


/** Empty the queue.
 */
void RfidReaderQueue_Empty(void);




#endif  /* _RFIDREADERQUEUE_H_ */
