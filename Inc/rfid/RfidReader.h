/***********************************************************
*
* RfidReader.h
*
* RFID reader driver.
*
* "Copyright (c) Digital Monitoring Products Inc. 2012-2016."
* "All rights reserved."
*
***********************************************************/

#ifndef RFIDREADER_H
#define RFIDREADER_H


#include <stdbool.h>
#include "Wiegand.h"




/** Structure to hold data that is serialized in ProgramData.
 */
typedef struct RfidReaderProgramData
{
  WiegandFormat_t format;

} RfidReaderProgramData_t;




/** Initialize the RFID reader.
 */
void RfidReader_Init(void);


/** Returns true if RFID reader has been initialized.
 */
bool RfidReader_IsInit(void);


/** De-initialize RFID reader.
 */
void RfidReader_DeInit(void);


/** Set the program data values.
 */
void RfidReader_SetProgramData(const WiegandFormat_t* wiegandformat);


/** Update the recent stimulus flag
 */
void RfidReader_SetRecentStimulus(bool value);


/** RFID reader process.
 *  Expected to be called periodically.
 */
void RfidReader_Process(void);




#endif // RFIDREADER_H
