/***********************************************************
*
* Wiegand.h
*
* Wiegand format and protocol structures.
*
* "Copyright (c) Digital Monitoring Products Inc. 2010-2013."
* "All rights reserved."
*
***********************************************************/

#ifndef _WIEGAND_H
#define _WIEGAND_H


#include <stdlib.h>
#include <stdint.h>


#define WIEGAND_MAX_USER_DIGITS    (10u)




typedef enum WiegandFormatOptionEnum
{
  WIEGANDFORMATOPTION_DMP,
  WIEGANDFORMATOPTION_CUSTOM,

} WiegandFormatOptionEnum_t;




/** Format information for a Wiegand stream of bits.
 */
typedef struct WiegandFormat
{
  uint8_t bitLength;         ///< total bit length

  uint8_t userCodeBitLen;    ///< length of bits in raw data.
  uint8_t userCodeBitPos;    ///< position of bits in raw data.
  uint8_t siteCodeBitLen;    ///< length of bits in raw data.
  uint8_t siteCodeBitPos;    ///< position of bits in raw data.

  WiegandFormatOptionEnum_t formatOption;

} WiegandFormat_t;




/** Structure to hold raw Wiegand data.
 */
typedef struct WiegandData
{
  uint64_t bits;
  uint64_t bit_count;

} WiegandData_t;


/** Reset values in WeigandData structure to zero.
 */
void WiegandData_Reset(WiegandData_t* self);


/** Set the WiegandFormat to the default DMP format.
 */
void WiegandFormat_SetToDefaultDMP(WiegandFormat_t* format);


/** Set the WiegandFormat to the default Custom format.
 */
void WiegandFormat_SetToDefaultCustom(WiegandFormat_t* format);


/** Extract user code digits from Wiegand data.
 */
size_t WiegandFormat_DataToUserDigits(uint64_t wiegandUserBits, char* user_digits, size_t user_digits_size);


/** Extract user data from the WiegandData bits.
 */
uint64_t WiegandFormat_GetUserData(const WiegandFormat_t* format, const WiegandData_t* data);


/** Extract site data from the WiegandData bits.
 */
uint64_t WiegandFormat_GetSiteData(const WiegandFormat_t* format, const WiegandData_t* data);




#endif // _WIEGAND_H
