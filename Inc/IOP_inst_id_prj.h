/*
 * IOP_inst_id_prj.h
 *
 *  Created by Bryce TeBeest on Mar 6, 2017
 *  Copyright (c) 2017 Cargt, LLC. All rights reserved.
 */

#if !defined(__IOP_INST_ID_PRJ_H__)
#define __IOP_INST_ID_PRJ_H__

#include "CARGT.h"
#include <stdint.h>

//IOP Instrument IDs
typedef uint16_t iop_inst_id_type;

enum
    {
    /*----------------------------------------------
     Digital Monitoring Products IOP instrument IDs
     ----------------------------------------------*/

    // From controller to button board
    IOP_BUTTON_PRESS                            = 1000,
    IOP_BUTTON_PRESS_RSP                        = 1001,
    IOP_BUTTON_RELEASE                          = 1002,
    IOP_BUTTON_RELEASE_RSP                      = 1003,
    IOP_POWER_OFF_REQUEST                       = 1004,
    IOP_POWER_OFF_REQUEST_RSP                   = 1005,

    IOP_GET_TEMPERATURE_MV                      = 1010,
    IOP_GET_TEMPERATURE_MV_RSP                  = 1011,
    IOP_GET_BATTERY_MV                          = 1012,
    IOP_GET_BATTERY_MV_RSP                      = 1013,

    IOP_LED_PATTERN                             = 1020,
    IOP_LED_PATTERN_RSP                         = 1021,
    IOP_LED_RATE                                = 1022,
    IOP_LED_RATE_RSP                            = 1023,
    IOP_LED_IR_ENABLE                           = 1024,
    IOP_LED_IR_ENABLE_RSP                       = 1025,
    IOP_LED_IR_SET_DRIVE_PERCENT                = 1026,
    IOP_LED_IR_SET_DRIVE_PERCENT_RSP            = 1027,

    IOP_BATTERY_TEMP_LOCKOUT_MV                 = 1028,
    IOP_BATTERY_TEMP_LOCKOUT_MV_RSP             = 1029,
    IOP_BATTERY_LOCKOUT_MV                      = 1030,
    IOP_BATTERY_LOCKOUT_MV_RSP                  = 1031,
    IOP_TEMP_LOCKOUT_MV                         = 1032,
    IOP_TEMP_LOCKOUT_MV_RSP                     = 1033,
    IOP_HEATER_SET_DRIVE_PERCENT                = 1034,
    IOP_HEATER_SET_DRIVE_PERCENT_RSP            = 1035,
    IOP_HEATER_SET_LOW_ENABLE_THRESHOLD         = 1036,
    IOP_HEATER_SET_LOW_ENABLE_THRESHOLD_RSP     = 1037,
    IOP_HEATER_SET_HIGH_DISABLE_THRESHOLD       = 1038,
    IOP_HEATER_SET_HIGH_DISABLE_THRESHOLD_RSP   = 1039,

    IOP_RFID_SCAN_EVENT                         = 1040,
    IOP_RFID_SCAN_EVENT_RSP                     = 1041,

    IOP_AMBIENT_GET_VALUE                       = 1050,
    IOP_AMBIENT_GET_VALUE_RSP                   = 1051,
    IOP_AMBIENT_SET_DAY_THRESHOLD               = 1052,
    IOP_AMBIENT_SET_DAY_THRESHOLD_RSP           = 1053,
    IOP_AMBIENT_SET_NIGHT_THRESHOLD             = 1054,
    IOP_AMBIENT_SET_NIGHT_THRESHOLD_RSP         = 1055,
    IOP_AMBIENT_DAY_THRESHOLD_OCCURRED_RSP      = 1056,
    IOP_AMBIENT_NIGHT_THRESHOLD_OCCURRED_RSP    = 1057,
    IOP_AMBIENT_GET_IS_DAY                      = 1058,
    IOP_AMBIENT_GET_IS_DAY_RSP                  = 1059,

    IOP_SHUTTER_SET_DAY_MODE                    = 1060,
    IOP_SHUTTER_SET_DAY_MODE_RSP                = 1061,
    IOP_SHUTTER_SET_NIGHT_MODE                  = 1062,
    IOP_SHUTTER_SET_NIGHT_MODE_RSP              = 1063,
    IOP_SHUTTER_SET_DRIVE_PERCENT               = 1064,
    IOP_SHUTTER_SET_DRIVE_PERCENT_RSP           = 1065,
    IOP_SHUTTER_SET_DRIVE_DURATION              = 1066,
    IOP_SHUTTER_SET_DRIVE_DURATION_RSP          = 1067,
    IOP_SHUTTER_SET_RELAY_ACTIVE_MS             = 1068,
    IOP_SHUTTER_SET_RELAY_ACTIVE_MS_RSP         = 1069,
    IOP_SHUTTER_SET_RELAY_PRE_DELAY_MS          = 1070,
    IOP_SHUTTER_SET_RELAY_PRE_DELAY_MS_RSP      = 1071,
    IOP_SHUTTER_SET_RELAY_POST_DELAY_MS         = 1072,
    IOP_SHUTTER_SET_RELAY_POST_DELAY_MS_RSP     = 1073,

    // From BLE_WIFI to controller
    IOP_START_WIFI_SCAN                         = 1100,
    IOP_START_WIFI_SCAN_RSP                     = 1101,
    IOP_AP_DETAILS_RQST                         = 1102,
    IOP_AP_DETAILS_RQST_RSP                     = 1103,
    IOP_AP_CONNECT_RQST                         = 1104,
    IOP_AP_CONNECT_RQST_RSP                     = 1105,
    IOP_AP_CONNECTION_INFO_RQST                 = 1106,
    IOP_AP_CONNECTION_INFO_RQST_RSP             = 1107,

    IOP_DEV_MAC_ADDR_RQST                       = 1120,
    IOP_DEV_MAC_ADDR_RQST_RSP                   = 1121,

	// From controller to RADIO_SPI

    //Start of next instrument ID group.
    IOP_INST_ID_LAST,                           //Must be last
    IOP_INST_ID_CNT                             = IOP_INST_ID_LAST
    };

//IOP_BUTTON_PRESS responses
enum
    {
    DOORBELL_DING_SUCCEEDED                     = 0,
    DOORBELL_DING_FAILED_BATT_TOO_LOW           = ( 1 << 0 ),
    DOORBELL_DING_FAILED_INVALID_TEMP           = ( 1 << 1 ),
    DOORBELL_DING_FAILED_BOOTING                = ( 1 << 2 ),
    DOORBELL_DING_HARDWIRED_INSTALLATION        = ( 1 << 3 ),
    DOORBELL_DING_BATTERY_NOT_PRESENT           = ( 1 << 4 ),
    };

/******************************************************************************
 * Message Data structures
 *****************************************************************************/
//IOP_BUTTON_PRESS
//IOP_BUTTON_PRESS_RSP
//IOP_BUTTON_RELEASE
//IOP_BUTTON_RELEASE_RSP
//IOP_POWER_OFF_REQUEST
//IOP_POWER_OFF_REQUEST_RSP

//IOP_GET_TEMPERATURE_MV
//IOP_GET_TEMPERATURE_MV_RSP
//IOP_GET_BATTERY_MV
//IOP_GET_BATTERY_MV_RSP

#pragma pack(1)
typedef struct
    {
	uint8_t     led_color;
    uint8_t     led_state;
    } IOP_led_color_pattern_state_type;
#pragma pack()

#pragma pack(1)
typedef struct
    {
	uint8_t     led_color;
    uint8_t     led_state;
    uint8_t     led_bits[2];
    } IOP_led_color_pattern_state_and_bits;
#pragma pack()

#pragma pack(1)
typedef struct
{
	uint8_t     led_color;
    uint8_t     led_state;
    uint8_t     led_bits[2];
    uint16_t    led_timeout_ms;
} IOP_led_color_pattern_state__and_bits_and_timeout_type;
#pragma pack()

//IOP_LED_RATE
//IOP_LED_RATE_RSP

//IOP_LED_IR_SET_DRIVE_PERCENT
//IOP_LED_IR_SET_DRIVE_PERCENT_RSP

//IOP_BATTERY_TEMP_LOCKOUT_MV
//IOP_BATTERY_TEMP_LOCKOUT_MV_RSP
//IOP_BATTERY_LOCKOUT_MV
//IOP_BATTERY_LOCKOUT_MV_RSP
//IOP_TEMP_LOCKOUT_MV
//IOP_TEMP_LOCKOUT_MV_RSP
//IOP_HEATER_SET_DRIVE_PERCENT
//IOP_HEATER_SET_DRIVE_PERCENT_RSP
//IOP_HEATER_SET_LOW_ENABLE_THRESHOLD
//IOP_HEATER_SET_LOW_ENABLE_THRESHOLD_RSP
//IOP_HEATER_SET_HIGH_DISABLE_THRESHOLD
//IOP_HEATER_SET_HIGH_DISABLE_THRESHOLD_RSP

//IOP_RFID_SCAN_EVENT
//IOP_RFID_SCAN_EVENT_RSP

//IOP_AMBIENT_GET_VALUE
//IOP_AMBIENT_GET_VALUE_RSP

#pragma pack(1)
typedef struct
    {
    uint8_t     upper8bits;
    uint8_t     lower8bits;
    } IOP_ambient_set_day_threshold_type;
#pragma pack()

//IOP_AMBIENT_SET_DAY_THRESHOLD_RSP

#pragma pack(1)
typedef struct
    {
    uint8_t     upper8bits;
    uint8_t     lower8bits;
    } IOP_ambient_set_night_threshold_type;
#pragma pack()

//IOP_AMBIENT_SET_NIGHT_THRESHOLD_RSP
//IOP_AMBIENT_DAY_THRESHOLD_OCCURRED_RSP
//IOP_AMBIENT_NIGHT_THRESHOLD_OCCURRED_RSP
//IOP_AMBIENT_GET_IS_DAY
//IOP_AMBIENT_GET_IS_DAY_RSP

//IOP_SHUTTER_SET_DAY_MODE
//IOP_SHUTTER_SET_DAY_MODE_RSP
//IOP_SHUTTER_SET_NIGHT_MODE
//IOP_SHUTTER_SET_NIGHT_MODE_RSP
//IOP_SHUTTER_SET_DRIVE_PERCENT
//IOP_SHUTTER_SET_DRIVE_PERCENT_RSP
//IOP_SHUTTER_SET_DRIVE_DURATION
//IOP_SHUTTER_SET_DRIVE_DURATION_RSP

#pragma pack(1)
typedef struct
    {
    uint8_t     upper8bits;
    uint8_t     lower8bits;
    } IOP_shutter_set_relay_active_ms_type;
#pragma pack()

//IOP_SHUTTER_SET_RELAY_ACTIVE_MS_RSP
//IOP_SHUTTER_SET_RELAY_PRE_DELAY_MS
//IOP_SHUTTER_SET_RELAY_PRE_DELAY_MS_RSP
//IOP_SHUTTER_SET_RELAY_POST_DELAY_MS
//IOP_SHUTTER_SET_RELAY_POST_DELAY_MS_RSP

#pragma pack(1)
typedef struct
    {
    uint8_t     led_ir_enable;
    } IOP_led_ir_enable_type;
#pragma pack()

// IOP_LED_IR_ENABLE_RSP - No data


#define MAX_SSID_LENGTH 32
#define MAX_PASSWORD_LENGTH 64
// BLE_WIFI messages

#define MAC_ADDR_LENGTH	( 24 )

// IOP_START_WIFI_SCAN - No data

#pragma pack(1)
typedef struct
    {
    uint16_t    number_of_access_points_found;
    uint8_t     connected_ssid[MAX_SSID_LENGTH];
    } IOP_start_wifi_scan_rsp_type;
#pragma pack()

#pragma pack(1)
typedef struct
    {
    uint16_t access_point_list_entry_number;
    } IOP_ap_details_rqst_type;
#pragma pack()

#pragma pack(1)
typedef struct
    {
    bool    connection_in_use;
    uint8_t num_bars;
    int32_t signal_strength;
    char    ssid[MAX_SSID_LENGTH];
    char    security[MAX_PASSWORD_LENGTH];
    } IOP_ap_details_rqst_rsp_type;
#pragma pack()

#pragma pack(1)
typedef struct
    {
    uint8_t ssid[MAX_SSID_LENGTH];
    uint8_t password_length;
    uint8_t password_type;
    uint8_t password_chars[MAX_PASSWORD_LENGTH];
    } IOP_ap_connect_rqst_type;
#pragma pack()

#pragma pack(1)
typedef struct
    {
    int16_t error_code;
    // connect details contained in info packet
    } IOP_ap_connect_rqst_rsp_type;
#pragma pack()

// IOP_AP_CONNECTION_INFO_RQST - No data

#pragma pack(1)
typedef struct
    {
    uint8_t is_connected;
    uint8_t ssid[MAX_SSID_LENGTH];
    } IOP_ap_connection_info_rqst_rsp_type;
#pragma pack()

// IOP_MAC_ADDRESS_RQST - No data

#pragma pack(1)
typedef struct
    {
    char addr[MAC_ADDR_LENGTH];
    } IOP_dev_mac_addr_rqst_rsp_type;
#pragma pack()

#endif /* __IOP_INST_ID_PRJ_H__ */
