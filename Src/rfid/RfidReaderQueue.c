/***********************************************************
*
* RfidReaderQueue.h
*
* RfidReaderQueue API.  A fixed length FIFO queue for WiegandDatas.
* Implemented as circular queue.
*
* "Copyright (c) Digital Monitoring Products Inc. 2012."
* "All rights reserved."
*
***********************************************************/


#include "RfidReaderQueue.h"
#include "Wiegand.h"
#include "AssertMacros.h"



/** Maximum number of elements in queue + 1.
 *  Note, to have 3 elements max, set to 4.
 */
#define RFIDREADERQUEUE_ELEMENT_COUNT    (4)




/** Simple event queue structure.
 *  Uses fixed size.
 */
typedef struct RfidReaderQueue
{
  WiegandData_t elements[RFIDREADERQUEUE_ELEMENT_COUNT];  ///< element container
  unsigned push_i;                                         ///< index of next element to be pushed
  unsigned pop_i;                                          ///< index of element to be popped

} RfidReaderQueue_t;


/** Singleton RfidReaderQueue.
 */
RfidReaderQueue_t RfidReaderQueue;




size_t RfidReaderQueue_GetMaxElements(void)
{
  // Can only use up to elements - 1 at one time
  return RFIDREADERQUEUE_ELEMENT_COUNT - 1;
}




size_t RfidReaderQueue_GetCount(void)
{
  size_t count = 0;
  unsigned i = RfidReaderQueue.pop_i;

  while (i != RfidReaderQueue.push_i)
  {
    ++count;
    ++i;
    if (i >= RFIDREADERQUEUE_ELEMENT_COUNT)
    {
      i = 0;
    }
  }

  return count;
}




bool RfidReaderQueue_Push(WiegandData_t event)
{
  unsigned next_i = RfidReaderQueue.push_i + 1;
  if (next_i >= RFIDREADERQUEUE_ELEMENT_COUNT)
  {
    next_i = 0;
  }

  if (next_i == RfidReaderQueue.pop_i)
  {
    ASSERT(0);
    return false;
  }

  RfidReaderQueue.elements[RfidReaderQueue.push_i] = event;

  RfidReaderQueue.push_i = next_i;

  return true;
}




WiegandData_t RfidReaderQueue_Pop()
{
  if (RfidReaderQueue_IsEmpty())
  {
    const WiegandData_t data = {.bits = 0, .bit_count = 0};
    return data;
  }

  WiegandData_t event = RfidReaderQueue.elements[RfidReaderQueue.pop_i];

  ++RfidReaderQueue.pop_i;

  if (RfidReaderQueue.pop_i >= RFIDREADERQUEUE_ELEMENT_COUNT)
  {
    RfidReaderQueue.pop_i = 0;
  }

  return event;
}




WiegandData_t RfidReaderQueue_Peek()
{
  if (RfidReaderQueue_IsEmpty())
  {
    const WiegandData_t data = {.bits = 0, .bit_count = 0};
    return data;
  }

  return RfidReaderQueue.elements[RfidReaderQueue.pop_i];
}




void RfidReaderQueue_Empty(void)
{
  RfidReaderQueue.push_i = 0;
  RfidReaderQueue.pop_i = 0;
}




bool RfidReaderQueue_IsEmpty(void)
{
  return RfidReaderQueue.push_i == RfidReaderQueue.pop_i;
}




void RfidReaderQueue_Init(void)
{
  RfidReaderQueue_Empty();
}
