/***********************************************************
*
* Wiegand.h
*
* Wiegand format and protocol structures.
*
* "Copyright (c) Digital Monitoring Products Inc. 2010-2013."
* "All rights reserved."
*
***********************************************************/


#include "Wiegand.h"
#include "AssertMacros.h"
#include "MinMax.h"




void WiegandFormat_SetToDefaultDMP(WiegandFormat_t* format)
{
  if (!format)
  {
    ASSERT_ERROR(__FILE__, __LINE__);
    return;
  }

  format->bitLength = 26;
  format->siteCodeBitPos = 1;
  format->siteCodeBitLen = 7;
  format->userCodeBitPos = 8;
  format->userCodeBitLen = 17;
}




void WiegandFormat_SetToDefaultCustom(WiegandFormat_t* format)
{
  if (!format)
  {
	ASSERT_ERROR(__FILE__, __LINE__);
	return;
  }

  format->bitLength = 26;
  format->siteCodeBitPos = 1;
  format->siteCodeBitLen = 8;
  format->userCodeBitPos = 9;
  format->userCodeBitLen = 16;
}




static uint64_t WiegandData_GetData(const WiegandData_t* data, size_t bit_pos, size_t bit_len)
{
  ASSERT(data);

  if ((bit_pos + bit_len) > data->bit_count)
  {
    if (bit_pos < data->bit_count)
    {
      bit_len = data->bit_count - bit_pos; // only use the bits we have
    }
    else
    {
      return 0;
    }
  }
  
  const uint64_t mask = (1 << bit_len) - 1;
  return (data->bits >> (data->bit_count - bit_pos - bit_len)) & mask;
}



void WiegandData_Reset(WiegandData_t* self)
{
  if (!self)
  {
    ASSERT_ERROR(__FILE__, __LINE__);
  }

  self->bit_count = 0;
  self->bits = 0;
}




uint64_t WiegandFormat_GetUserData(const WiegandFormat_t* format, const WiegandData_t* data)
{
  ASSERT(format);
  ASSERT(data);

  return WiegandData_GetData(data, format->userCodeBitPos, format->userCodeBitLen);
}




uint64_t WiegandFormat_GetSiteData(const WiegandFormat_t* format, const WiegandData_t* data)
{
  ASSERT(format);
  ASSERT(data);

  return WiegandData_GetData(data, format->siteCodeBitPos, format->siteCodeBitLen);
}




size_t WiegandFormat_DataToUserDigits(uint64_t wiegandUserBits, char* user_digits, size_t user_digits_size)
{
  ASSERT(user_digits);
  ASSERT(user_digits_size);

  uint32_t tempWiegandUserBits = wiegandUserBits;

  const size_t user_digit_count = MIN(user_digits_size, WIEGAND_MAX_USER_DIGITS);
  size_t i = user_digit_count;
  do
  {
    --i;
    user_digits[i] = (tempWiegandUserBits % 10) + '0';
    tempWiegandUserBits /= 10;
  } while (i);

  return user_digit_count;
}
