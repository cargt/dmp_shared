/***********************************************************
*
* RfidReader.c
*
* RFID reader driver.
*
* "Copyright (c) Digital Monitoring Products Inc. 2013-2017."
* "All rights reserved."
*
***********************************************************/



/*

Decoding RFID
---------------------

- CPU timers use PCLK running at 18 MHz (T = 55.6 ns)
- CPU generates 125 kHz (T = 8 us) square wave output signal.
- RFID circuit generates 125 kHz sinusoidal carrier wave.
- Carrier wave is FSK 8/10 modulated by RFID tag.
- RFID circuit generates FSK 8/10 square wave signal from amplitude modulated carrier wave.
- CPU receives FSK 8/10 modulated square wave.


64 us --------->| Received 0
--------________--------________

72 us ----------->| ? Out of sync
--------__________----------

80 us ------------->| Received 1
----------__________----------__________

*/


#include "stm32f0xx_hal.h"
#include "doorbell.h"

#include "RfidReader.h"

#include <string.h>

#include "AssertMacros.h"
#include "RfidReaderQueue.h"
#include "Wiegand.h"



#define RFIDTIMER_CAPTURE_TICKS_PER_SECOND    48000000
#define RFIDREADER_CARRIER_HZ                 (125000)
#define RFIDREADER_CARRIER_TICKS_PER_CYCLE    (RFIDTIMER_CAPTURE_TICKS_PER_SECOND / RFIDREADER_CARRIER_HZ)

// Using FSK 8/10 the pulse widths are 64us and 80us
#define RFIDREADER_FSK_MIDTICKS         (RFIDREADER_CARRIER_TICKS_PER_CYCLE * 9) // 72us

#define RFIDREADER_CARRIER_OFF_NO_DATA          (160u)    /* 80 mS    Carrier off, no data */
#define RFIDREADER_CARRIER_OFF_DATA_DETECTED    (952u)    /* 476 mS   Carrier off, data detected */
#define RFIDREADER_CARRIER_ON_SETTLE            (3u)      /* 1.5 mS   Wait until carrier settles */
#define RFIDREADER_CARRIER_ON_DATA_SCAN         (35u)     /* 17.5 mS  Look for data from card */
#define RFIDREADER_CARRIER_ON_DATA_DETECTED     (300u)    /* 150 mS   Data detected, wait for all */

#define RFIDREADER_HID_PREAMBLE            0x1d
#define RFIDREADER_ONES_COUNT_THRESHOLD       4
#define RFIDREADER_ZEROES_COUNT_THRESHOLD     5
  

#define RFIDREADER_HID_TOTAL_BITS     44

#define RFID_POWER_FAIL_TIMEOUT  (3600 * SEC_TO_100MS)  // 1 hour


/** RFID state machine flags.
 */
typedef struct RfidFlags
{
  uint32_t preambleFound       : 1;
  uint32_t dataDetected        : 1;
  uint32_t dataComplete        : 1;
  uint32_t extendedPowerFail   : 1;
  uint32_t recentStimulus      : 1;
  uint32_t enabled             : 1;
  uint32_t                     : 26;

} RfidFlags_t;


typedef struct RfidReader
{
  RfidReaderProgramData_t programData;

  RfidFlags_t flags;

  WiegandData_t data;
  WiegandData_t lastData[2];

  uint32_t carrierTimerCounter;
  uint8_t carrierStateCntr;
  
  uint8_t preamble_bits;
  
  uint32_t lastTick;
  uint8_t ones_count;
  uint8_t zeroes_count;

  uint8_t detectCntr;

  bool initialized;

} RfidReader_t;
static RfidReader_t RfidReader;


static void RfidReader_PowerFailTimeout(void const * argument);

static void RfidTimers_SetEnable125Khz(bool value)
{
if(value)
  {
  HAL_TIM_PWM_Start(F125KHZ_TIMER, TIM_CHANNEL_1);
  }
else
  {
  HAL_TIM_PWM_Stop(F125KHZ_TIMER, TIM_CHANNEL_1);
  }
}

static void RfidTimers_SetEnableCapture(bool value)
{
if(value)
  {
  HAL_NVIC_EnableIRQ(EXTI0_1_IRQn);
  }
else
  {
  HAL_NVIC_DisableIRQ(EXTI0_1_IRQn);
  }
}

static void RfidReader_SetDataDetected(bool value)
{
  RfidReader.flags.dataDetected = value;
}

static void RfidReader_SetDataComplete(bool value)
{
  RfidReader.flags.dataComplete = value;
}


static void RfidReader_SetPreambleFound(bool value)
{
  RfidReader.flags.preambleFound = value;
}

static void RfidReader_Reset(void)
{
  RfidReader_SetPreambleFound(false);
  RfidReader_SetDataDetected(false);
  RfidReader_SetDataComplete(false);
  RfidReader.ones_count = 0;
  RfidReader.zeroes_count = 0;
  RfidReader.preamble_bits = 0;
}

static void RfidReader_DecoderReset(void)
{
  WiegandData_Reset(&RfidReader.data);
  RfidReader.preamble_bits = 0;
  RfidReader_SetPreambleFound(false);
}


static void RfidReader_OnDecodedBit(uint32_t bit)
{
  static uint32_t last_queue_push = 0;

  if (RfidReader.flags.dataComplete)
  {
    return;
  }

  RfidReader.data.bits <<= 1;
  RfidReader.data.bits |= bit & 0x01;
  
  if (++RfidReader.data.bit_count >= RFIDREADER_HID_TOTAL_BITS)
  {
    if ((0 == memcmp(&RfidReader.data, &RfidReader.lastData[0], sizeof(RfidReader.data))) ||
        (0 == memcmp(&RfidReader.data, &RfidReader.lastData[1], sizeof(RfidReader.data))) )
    {
      // read is the same
      uint32_t current_time = Get_Time_RFID_Reader();
      if( ( current_time >= last_queue_push + RFIDREADER_CARRIER_OFF_DATA_DETECTED ) ||
          ( current_time < RFIDREADER_CARRIER_OFF_DATA_DETECTED ) ||
          ( current_time < last_queue_push ) )
      {
        //Wait for a known timeout to occur or uint32_t rollover
        RfidReaderQueue_Push(RfidReader.data);

        WiegandData_Reset(&RfidReader.lastData[0]);
        WiegandData_Reset(&RfidReader.lastData[1]);
        RfidReader_DecoderReset();
        RfidReader_SetDataComplete(true);
        RfidTimers_SetEnableCapture(false);
        RfidTimers_SetEnable125Khz(false);

        // generate an event to propagate the RFID data into the system
        Doorbell_Generate_Event( RFID_DETECTED_EVENT );
        last_queue_push = current_time;
      }
    }
    else
    {
      // save this read data to compare next time
      memcpy(&RfidReader.lastData[1], &RfidReader.lastData[0], sizeof(WiegandData_t));
      memcpy(&RfidReader.lastData[0], &RfidReader.data, sizeof(WiegandData_t));
    }
  }
}


static void RfidReader_ManchesterDemodulate(uint32_t fskBit)
{
  static bool first_half = false;
  static uint32_t first_value = 0;
  
  if (!RfidReader.flags.preambleFound)
  {
    RfidReader.preamble_bits <<= 1;
    RfidReader.preamble_bits |= fskBit & 0x01;
    
    if (RFIDREADER_HID_PREAMBLE == RfidReader.preamble_bits)
    {
      RfidReader.preamble_bits = 0;
      RfidReader_SetPreambleFound(true);
      first_half = true;
    }
  }
  else if (first_half)
  {
    first_value = fskBit;
    first_half = false;
  }
  else if (first_value != fskBit)
  {
    first_half = true;
    RfidReader_OnDecodedBit(first_value);
  }
  else  // decoding error
  {
    RfidReader_DecoderReset();
  }
}

static void RfidReader_OnFskBit(uint32_t fskBit)
{
  if (RfidReader.flags.dataDetected)
  {
    RfidReader_ManchesterDemodulate(fskBit);
  }
  else
  {
    // detected data, sync ManchesterDemodulator with data stream
    RfidReader_SetDataDetected(true);
    RfidReader_DecoderReset();
  }
}


/** Called by RfidTimers OnCapture callback.
 */
void RfidReader_OnCapture(uint32_t tick)
{
  if ((tick - RfidReader.lastTick) >= RFIDREADER_FSK_MIDTICKS)
  {
    RfidReader.zeroes_count = 0;
    if (++RfidReader.ones_count >= RFIDREADER_ONES_COUNT_THRESHOLD)
    {
      RfidReader_OnFskBit(1);
      RfidReader.ones_count = 0;
    }
  }
  else
  {
    RfidReader.ones_count = 0;
    if (++RfidReader.zeroes_count >= RFIDREADER_ZEROES_COUNT_THRESHOLD)
    {
      RfidReader_OnFskBit(0);
      RfidReader.zeroes_count = 0;
    }
  }
  RfidReader.lastTick = tick;
}


void RfidReader_OnCarrierTimer(void)
{
  static uint8_t carrier_off_counter = 0;

  if (0 == RfidReader.carrierTimerCounter)
  {
    return;
  }

  --RfidReader.carrierTimerCounter;

  if (0 != RfidReader.carrierTimerCounter)
  {
    return;
  }

  switch (RfidReader.carrierStateCntr)
  {
    case 0:
    {
      RfidTimers_SetEnableCapture(false);
      RfidTimers_SetEnable125Khz(false);

      if (RfidReader.flags.dataDetected)
      {
        if( carrier_off_counter >= 8 )
          {
          //Allow full de-energize to occur
          RfidReader.carrierTimerCounter = RFIDREADER_CARRIER_OFF_DATA_DETECTED;
          carrier_off_counter = 1;
          }
        else
          {
          //Use a short pause instead to allow for faster card acquisition than once every 0.5 seconds (at best).
          RfidReader.carrierTimerCounter = RFIDREADER_CARRIER_OFF_NO_DATA;
          carrier_off_counter++;
          }
      }
      else
      {
        RfidReader.carrierTimerCounter = RFIDREADER_CARRIER_OFF_NO_DATA;
      }

      ++RfidReader.carrierStateCntr;
    } break;

    case 1:
    {
      //Start carrier and wait for tank circuit to stabilize
      RfidTimers_SetEnable125Khz(true);
      RfidReader.carrierTimerCounter = RFIDREADER_CARRIER_ON_SETTLE;
      ++RfidReader.carrierStateCntr;
    } break;

    case 2:
    {
      // Tank circuit stable, start looking for data
      RfidReader_Reset();
      RfidTimers_SetEnableCapture(true);
      RfidReader.carrierTimerCounter = RFIDREADER_CARRIER_ON_DATA_SCAN;
      ++RfidReader.carrierStateCntr;

    } break;

    case 3:
    {
      // if data found,extend carrier on time to read all data.
      if (RfidReader.flags.dataDetected)
      {
        RfidReader.carrierTimerCounter = RFIDREADER_CARRIER_ON_DATA_DETECTED;
      }
      else
      {
        RfidReader.carrierTimerCounter = 1;
      }

      RfidReader.carrierStateCntr = 0;
    } break;

    default:
    {
      ASSERT_ERROR(__FILE__, __LINE__);
      RfidReader.carrierStateCntr = 0;
    }
  }
}


/*F********************************************************************************************
*
* Name: RfidReader_PowerFailTimeout
* Description:  This function is called when the power fail timer expires.
* Parameters:   pointer to data - not used
* Return Value: None
*********************************************************************************************F*/
static void RfidReader_PowerFailTimeout(void const * argument)
{
  RfidReader.flags.extendedPowerFail = true;
}



/*F********************************************************************************************
*
* Name: RfidReader_Disable
* Description:  This function disables the rfid reader.
* Parameters:   None
* Return Value: None
*********************************************************************************************F*/
static void RfidReader_Disable(void)
{
  RfidReader.carrierTimerCounter = 0;  //stop carrier state machine
  RfidTimers_SetEnableCapture(false);
  RfidTimers_SetEnable125Khz(false);
  RfidReader.flags.enabled = false;
}



/*F********************************************************************************************
*
* Name: RfidReader_Enable
* Description:  This function enables the rfid reader.
* Parameters:   None
* Return Value: None
*********************************************************************************************F*/
static void RfidReader_Enable(void)
{
  RfidReader_Reset();
  RfidReader.carrierStateCntr = 0;
  RfidReader.carrierTimerCounter = 1;
  RfidReader.flags.enabled = true;
}



/*F********************************************************************************************
*
* Name: RfidReader_SetRecentStimulus
* Description:  This function updates the recent stimulus flag.
* Parameters:   the new flag setting
* Return Value: None
*********************************************************************************************F*/
void RfidReader_SetRecentStimulus(bool value)
{
  RfidReader.flags.recentStimulus = value;
}



void RfidReader_Process(void)
{
  #if defined(KEYPAD_9800)
  
  static bool was_AC_power_fail = false;
  bool now_AC_power_fail = PowerManager_IsPowerFail();
  
  if (now_AC_power_fail != was_AC_power_fail)
  {
    if (now_AC_power_fail)
    {
      //osTimerStart(rfidPowerFailTimer, RFID_POWER_FAIL_TIMEOUT);
    }
    else
    {
      //osTimerStop(rfidPowerFailTimer);
      RfidReader.flags.extendedPowerFail = false;
    }
    was_AC_power_fail = now_AC_power_fail;
  }
  
  if (RfidReader.flags.extendedPowerFail && !RfidReader.flags.recentStimulus && RfidReader.flags.enabled)
  {
    RfidReader_Disable();
  }
  else if (!RfidReader.flags.enabled && (!RfidReader.flags.extendedPowerFail || RfidReader.flags.recentStimulus))
  {
    RfidReader_Enable();
  }
  
  #endif
  
}




void RfidReader_SetProgramData(const WiegandFormat_t* wiegandformat)
{
  if (!wiegandformat)
  {
    ASSERT_ERROR(__FILE__, __LINE__);
    return;
  }

  RfidReader.programData.format = *wiegandformat;
}




bool RfidReader_IsInit(void)
{
  return RfidReader.initialized;
}




void RfidReader_DeInit(void)
{
  RfidReader_Disable();
  RfidReader.initialized = false;
}




/*************************************************************************
 * Function Name: RfidReader_Init
 * Parameters: none
 *
 * Return: none
 *
 * Description: Initialize prox reader
 *
 *************************************************************************/
void RfidReader_Init(void)
{
  if (RfidReader.initialized)
  {
    return;
  }

  memset(&RfidReader, 0, sizeof(RfidReader));
  WiegandFormat_SetToDefaultDMP(&RfidReader.programData.format);

  HAL_TIM_Base_Start_IT( F125KHZ_TIMER );
  HAL_TIM_Base_Start_IT( RFID_STATE_MACHINE_TIMER );

  RfidReader_Enable();

  RfidReader.initialized = true;
}

